const express = require('express')
//create and application using express
//this creates and express application and stores this as a constant called 'app'
//'app' is our server
const app = express()
//we need a port to listen to
const port = 3000

//setup for allowing the server to handle data from requests
//allows your app to read json
//middlewares - softwares that provides services outside of what's offered by the operating system
app.use(express.json())
//allows your app to read data from froms
//be default, the information received from the url can only be received as a string or an array
//extended:true allows us to receieve other data types
app.use(express.urlencoded({extended:true}))

//we will create routes
//express has methods corresponding to each HTTP method
//this route expects to receive a GET request at the base URI '/'
// '/' = 'http://localhost:3000'
app.get('/', (req, res) => {
	//res.send uses the express JS moduele's method instead to send a response back to the client
	res.send("Hello World")
})

app.post('/hello', (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

//for /signup
//mock database
let users = []

app.post('/signup', (req,res)=>{
	console.log(req.body)
	//if contents of the request body with the property 'username' and 'password' is not empty
	if(req.body.username !== '' && req.body.password !== ''){
		//this will store the user object sent via POSTMAN to the users array created above
		users.push(req.body)
		//send response
		res.send(`User ${req.body.username} successfully registered!`)
	}
	else{
		res.send('Please input BOTH username and password')
	}
})

//update the password of a user that matches the information provided in the client/POSTMAN
app.put('/change-password', (req,res)=>{
	console.log(req.body)
	let message;

	for(let i=0;i<users.length;i++){
		if(req.body.username==users[i].username){

		users[i].password = req.body.password

		message = `User ${req.body.username}'s password has been updated`
		break;
		}
		else{
			message = "User does not exist"
		}
	}
	res.send(message)
})

//Welcome to the home page
//hint: ano method pambura ng isang value splice

//-----
//1. Create a GET route that will access the "/home" route that will print out a simple message.
app.get('/home', (req, res) => {
	res.send("Welcome to the home page")
})

//3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get('/users', (req, res) => {
	let message
	for(let i=0;i<users.length;i++){
			message += `${users[i].username}, ${users[i].password}\n`
	}
	res.send(message)	
})

//5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete('/delete-user', (req, res) => {
	res.send(`User ${users[0].username} has been deleted.`)
	users.splice(0,1)
})



app.listen(port, () => console.log(`I love you ${port} - Tony Stark`))

